<?php

namespace Drupal\FreeRideSiteMap\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'FreeRideSiteMap' Block.
 *
 * @Block(
 *   id = "FreeRideSiteMap_Block",
 *   admin_label = @Translation("FreeRideSiteMap Block"),
 *   subject = @Translation("FreeRideSiteMap Block"),
 * )
 */
class FreeRideSiteMapBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build= array(
      '#type' => 'markup',
      '#markup' => '<h1>Hello</h1>',
    	);
    return $build;
  }

}