<?php

namespace Drupal\AthenaSiteMap\Controller;

use Drupal\Core\Controller\ControllerBase;

class AthenaSiteMapController extends ControllerBase {

  public function content() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t("Hello, world!"),
    ];
  }

}